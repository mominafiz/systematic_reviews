import numpy as np
from sklearn.ensemble import BaggingClassifier
from sklearn.neighbors import KNeighborsClassifier

from sklearn.metrics import accuracy_score, precision_score, \
        recall_score, auc, roc_curve, f1_score, classification_report, roc_auc_score
from sklearn.cross_validation import train_test_split, KFold
import sys


# for header
nrow = -1
ncol = 0
featnames = []
FILE = "oralhypoglycemics_30.csv"

with open(FILE,"r") as infile:
    for row in infile:
        nrow = nrow + 1
        if ncol == 0:
            featnames = row.strip().split(",")
            ncol = len(featnames)

data = np.array(-1 * np.ones((nrow,ncol), float),object)

flag = True
with open(FILE,"r") as infile:
    k = 0
    for row in infile:
        if flag == True:
            flag = False
            continue
        else:
            data[k,:] = np.array(row.strip().split(","))
            k = k + 1
           

featnames = np.array(featnames,str)

# create vectors
keys = [[]] * np.size(data,1)

# fill matrix with -1s
numdata = -1 * np.ones_like(data)


for k in range(np.size(data,1)):
    keys[k],garbage,numdata[:,k] = np.unique(data[:,k],True,True)


numdata = np.array(numdata,int)
xdata = numdata[:,:-1]  # all data, but class
ydata = numdata[:,-1]    # class label

result_headers = ["Accuracy", "Precision", "Recall", "F-Score", "ROC"]
kfold = KFold(nrow,10,random_state=42)
scores = []

## ------ for roc curve -----
#fpr = dict()
#tpr = dict()
#roc_auc = dict()

## --------------------------

for train,test in kfold:
    Xtrain, Xtest, ytrain, ytest = train_test_split(xdata,ydata, test_size=0.33, random_state=42)
    
    knn = KNeighborsClassifier()
    adaBoost = BaggingClassifier(knn)
    adaBoostfit = adaBoost.fit(Xtrain, ytrain)
    ypred = adaBoost.predict(Xtest)

    results = []    
    accuracy = accuracy_score(ytest,ypred)
    results.append(accuracy*100)
    
    results.append(precision_score(ytest,ypred)*100)
    results.append(recall_score(ytest,ypred)*100)
    results.append(f1_score(ytest,ypred)*100)
    results.append(roc_auc_score(ytest,ypred)*100)
    scores.append(results)

    #y_score = adaBoostfit.decision_function(Xtest)
    #for i in range(n_classes):
    #    fpr[i], tpr[i], _ = roc_curve(ytest[:, i], y_score[:, i])
    #    roc_auc[i] = auc(fpr[i], tpr[i])
    

avg_results = []
for score in zip(*scores):
    avg = str(reduce((lambda x,y: x+y),score)/float(10))
    avg_results.append(avg)

print "------------------------------------------------"
print "Bagging with KNN (10 fold cross validation) results: \n"
print "-------------------------------------------------"
for tup in zip(result_headers,avg_results):
    print str.format("{0}:\t{1}",tup[0],tup[1])

sys.exit(0)

"""
# --------------------------------------------------------------------------
# ----------------------------- Implement ROC -----------------------------
# ref:http://scikit-learn.org/stable/auto_examples/model_selection/plot_roc.html
# ------------------------------------------------------------------------------

from scipy import interp



# Compute macro-average ROC curve and ROC area

# First aggregate all false positive rates
n_classes = 2
all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))

# Then interpolate all ROC curves at this points
mean_tpr = np.zeros_like(all_fpr)
for i in range(n_classes):
    mean_tpr += interp(all_fpr, fpr[i], tpr[i])

# Finally average it and compute AUC
mean_tpr /= n_classes

fpr["macro"] = all_fpr
tpr["macro"] = mean_tpr
roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

# Plot all ROC curves
plt.figure()
plt.plot(fpr["micro"], tpr["micro"],
         label='micro-average ROC curve (area = {0:0.2f})'
               ''.format(roc_auc["micro"]),
         linewidth=2)

plt.plot(fpr["macro"], tpr["macro"],
         label='macro-average ROC curve (area = {0:0.2f})'
               ''.format(roc_auc["macro"]),
         linewidth=2)

for i in range(n_classes):
    plt.plot(fpr[i], tpr[i], label='ROC curve of class {0} (area = {1:0.2f})'
                                   ''.format(i, roc_auc[i]))

plt.plot([0, 1], [0, 1], 'k--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Some extension of Receiver operating characteristic to multi-class')
plt.legend(loc="lower right")
plt.show()

print
print "ROC:"
"""