import numpy as np
import sys
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score, precision_score, \
        recall_score, auc, roc_curve, f1_score, classification_report, roc_auc_score
from sklearn.cross_validation import train_test_split, KFold

# for header
nrow = -1
ncol = 0
featnames = []
FILE = "oralhypoglycemics_30.csv"

with open(FILE,"r") as infile:
    for row in infile:
        nrow = nrow + 1
        if ncol == 0:
            featnames = row.strip().split(",")
            ncol = len(featnames)

data = np.array(-1 * np.ones((nrow,ncol), float),object)

flag = True
with open(FILE,"r") as infile:
    k = 0
    for row in infile:
        if flag == True:
            flag = False
            continue
        else:
            data[k,:] = np.array(row.strip().split(","))
            k = k + 1
           

featnames = np.array(featnames,str)

# create vectors
keys = [[]] * np.size(data,1)

# fill matrix with -1s
numdata = -1 * np.ones_like(data)


for k in range(np.size(data,1)):
    keys[k],garbage,numdata[:,k] = np.unique(data[:,k],True,True)


numdata = np.array(numdata,int)
xdata = numdata[:,:-1]  # all data, but class
ydata = numdata[:,-1]    # class label

result_headers = ["Accuracy", "Precision", "Recall", "F-Score","ROC"]
kfold = KFold(nrow,10,random_state=42)
scores = []

for train,test in kfold:
    Xtrain, Xtest, ytrain, ytest = train_test_split(xdata,ydata, test_size=0.33, random_state=42)
    
    dt = DecisionTreeClassifier(random_state=42)
    dt.fit(Xtrain, ytrain)
    ypred = dt.predict(Xtest)

    results = []    
    accuracy = accuracy_score(ytest,ypred)
    results.append(accuracy*100)
    
    results.append(precision_score(ytest,ypred)*100)
    results.append(recall_score(ytest,ypred)*100)
    results.append(f1_score(ytest,ypred)*100)
    results.append(roc_auc_score(ytest,ypred)*100)
    scores.append(results)

avg_results = []
for score in zip(*scores):
    avg = str(reduce((lambda x,y: x+y),score)/float(10))
    avg_results.append(avg)

print "------------------------------------------------"
print "Decision Tree using CART (10 fold cross validation) results: \n"
print "-------------------------------------------------"
for tup in zip(result_headers,avg_results):
    print str.format("{0}:\t{1}",tup[0],tup[1])

sys.exit(0)