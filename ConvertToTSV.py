﻿import re
import sys
import os

def checkArgs(args):
    if len(args) == 3 and pathExists(args[-2]):
        return args[-2], args[-1]
    else:
        print "Usage: python PythonApplication.py <input filepath> <output filepath>"
        sys.exit(1)
 
def pathExists(path):
    if os.path.exists(path):
        return True
    else: 
        print "File path does not exists."
        sys.exit(1)
          

if __name__ == "__main__":
    
    inpath,outpath = checkArgs(sys.argv)

    with open(inpath,"r") as infile, open(outpath,"w") as outfile:
        header = ['Document Id','Title', 'Abstract', 'PubMed Tag','MeSH Tag','Class']
        outfile.write('\t'.join(header) + '\n')
        
        instance = None
        
        for lines in infile.readlines():         
            instance = ['NA' for i in xrange(6)]  
            for line in re.split("\\r+",lines):

                if re.match("\*+\\s+",line) is not None: 
                    entries = re.split("\\s+", line, 1)
                
                    if entries[1].strip().isdigit():
                        instance[0] = entries[1].strip()
                   
                
                elif re.match("-+K\\s+",line) is not None:
                    entries = re.split("-+K\\s+", line, 1)
                
                    kclass = entries[-1].strip()
                    if not(kclass == "" or kclass is None or kclass.isdigit()):
                        instance[-1] = kclass

                elif re.match("-+T\\s+", line) is not None:
                    entries = re.split("-+T\\s+",line,1)
                    title = entries[-1].strip()

                    if not (title == "" or title is None):
                        instance[1] = title

                elif re.match("-+A\\s+", line) is not None:
                    entries = re.split("-+A\\s+",line,1)
                    abstract = entries[-1].strip()

                    if not(abstract == "" or abstract is None):
                        instance[2] = abstract

                elif re.match("-+P\\s+", line) is not None:
                    entries = re.split("-+P\\s+",line,1)
                    pub = entries[-1].strip()

                    if not(pub == "" or pub is None):                        
                        instance[3] = pub

                elif re.match("-+M\\s+", line) is not None:
                    entries = re.split("-+M\\s+",line,1)
                    mesh = entries[-1].strip()

                    if not(mesh == "" or mesh is None):
                        instance[4] = mesh
                    
            outfile.write('%'.join(instance) + '\n')

            #print '\t'.join(instance) + '\n'
            #raw_input(">")