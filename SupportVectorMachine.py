import csv
import numpy as np
from sklearn.preprocessing import LabelBinarizer
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score, precision_score, \
        recall_score, auc, roc_curve, f1_score, classification_report, roc_auc_score
from sklearn.cross_validation import train_test_split, KFold


import matplotlib.pyplot as plt
import pandas as pd
import sys

# for header
nrow = -1
ncol = 0
featnames = []
FILE = "oralhypoglycemics_30.csv"

with open(FILE,"r") as infile:
    for row in infile:
        nrow = nrow + 1
        if ncol == 0:
            featnames = row.strip().split(",")
            ncol = len(featnames)

data = np.array(-1 * np.ones((nrow,ncol), float),object)

flag = True
with open(FILE,"r") as infile:
    k = 0
    for row in infile:
        if flag == True:
            flag = False
            continue
        else:
            data[k,:] = np.array(row.strip().split(","))
            k = k + 1
           

featnames = np.array(featnames,str)

# create vectors
keys = [[]] * np.size(data,1)

# fill matrix with -1s
numdata = -1 * np.ones_like(data)


for k in range(np.size(data,1)):
    keys[k],garbage,numdata[:,k] = np.unique(data[:,k],True,True)



numdata = np.array(numdata,int)
xdata = numdata[:,:-1]  # all data, but class
ydata = numdata[:,-1]    # class label
"""
#----------------- numdata multilabel --> binary conversion in xdata --------
lbin = LabelBinarizer()
for k in range(np.size(xdata,1)):
    if k==0:
        xdata_ml = lbin.fit_transform(xdata[:,k])
    else:
        xdata_ml = np.hstack((xdata_ml, lbin.fit_transform(xdata[:,k])))
ydata_ml = lbin.fit_transform(ydata)

print xdata_ml
"""

result_headers = ["Accuracy", "Precision", "Recall", "F-Score","ROC"]
kfold = KFold(nrow,10,random_state=42)
scores = []

for train,test in kfold:
    Xtrain, Xtest, ytrain, ytest = train_test_split(xdata,ydata, test_size=0.33, random_state=42)
    svm = SGDClassifier(loss="hinge",penalty="l2",alpha=0.0001, n_iter=5, random_state=42)
    svm.fit(Xtrain,ytrain)
    ypred = svm.predict(Xtest)

    results = []    
    accuracy = accuracy_score(ytest,ypred)
    results.append(accuracy*100)
    
    results.append(precision_score(ytest,ypred)*100)
    results.append(recall_score(ytest,ypred)*100)
    results.append(f1_score(ytest,ypred)*100)
    results.append(roc_auc_score(ytest,ypred)*100)
    scores.append(results)

avg_results = []
for score in zip(*scores):
    avg = str(reduce((lambda x,y: x+y),score)/float(10))
    avg_results.append(avg)

print "SVM results: \n"
for tup in zip(result_headers,avg_results):
    print str.format("{0}:\t{1}",tup[0],tup[1])

##print scores
#with open(FILE+"_svm_results.csv","w") as outfile:
#    outfile.write(",".join(result_headers)+'\n')
#    outfile.write(",".join(avg_results)+'\n')


